import datetime
from django.db import models
from django.utils import timezone

class RegistrationEntry(models.Model):
    class Meta:
        verbose_name_plural = "Registration Entries"    
    
    given_name = models.CharField(max_length = 64)
    middle_name = models.CharField(max_length = 64)
    last_name = models.CharField(max_length = 64)
    birth_date = models.DateField()
    MALE = 'M'
    FEMALE = 'F'
    gender_choices = (
        (MALE, 'Male'),
        (FEMALE, 'Female'),
    )
    gender = models.CharField(
        max_length = 4,
        choices=gender_choices,
    )
    his_id = models.CharField(max_length = 128)
    visit_number = models.CharField(max_length = 128)
    current_time = datetime.datetime.now()
    created = models.DateTimeField(auto_now= True)
    last_modified = models.DateTimeField(auto_now= True)
    triage_category = models.CharField(max_length = 64)
    computer_name = models.CharField(max_length = 64)
    FOR_DELETION = -1
    DRAFT = 0
    FOR_UPDATE = 1
    record_state_choices = (
        (FOR_DELETION, 'For Deletion'),
        (DRAFT, 'Draft'),
        (FOR_UPDATE, 'For Update')
    )
    record_state = models.SmallIntegerField(choices=record_state_choices)

    def __str__(self):
        return self.given_name 

class ServiceUpdateEntry(models.Model):
    class Meta:
        verbose_name_plural = "Service Update Entries"

    his_id = models.CharField(max_length = 128)
    visit_number = models.CharField(max_length = 128)
    triage_category = models.CharField(max_length = 64)
    bed_number = models.CharField(max_length = 64)

    def __str__(self):
        return self.his_id 

class PaymentUpdateEntry(models.Model):
    class Meta:
        verbose_name_plural = "Payment Update Entries"

    his_id = models.CharField(max_length = 128)
    visit_number = models.CharField(max_length = 128)
    payment_method = models.CharField(max_length = 64)

    def __str__(self):
        return self.his_id 
