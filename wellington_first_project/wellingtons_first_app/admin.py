from django.contrib import admin
from wellingtons_first_app.models import RegistrationEntry, ServiceUpdateEntry, PaymentUpdateEntry

class RegistrationEntry_Admin(admin.ModelAdmin):
    pass
    admin.site.register(RegistrationEntry)

class ServiceUpdateEntry_Admin(admin.ModelAdmin):
    pass
    admin.site.register(ServiceUpdateEntry)

class PaymentUpdateEntry_Admin(admin.ModelAdmin):
    pass
    admin.site.register(PaymentUpdateEntry)