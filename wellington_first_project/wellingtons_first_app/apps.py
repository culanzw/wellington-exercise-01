from django.apps import AppConfig


class WellingtonsFirstAppConfig(AppConfig):
    name = 'wellingtons_first_app'
