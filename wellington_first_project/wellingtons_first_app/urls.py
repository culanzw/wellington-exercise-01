from django.urls import path
from . import views

app_name = 'wellingtons_first_app'
urlpatterns = [
    path('', views.index, name='index'),
]